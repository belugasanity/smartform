<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elements extends Model
{
    protected $fillable = ['name','email','category','priority-low','priority-normal','priority-high','copy','human','message'];
}
