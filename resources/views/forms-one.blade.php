@extends('layouts.master')
@section('title', 'Form data 1')
@section('content')
	<div id="main">
		<h2>Entries from the footer contact form</h2>
		<div class="table-wrapper">
		<table>
		@foreach ($messages as $message)
			<tr>
				<td>{{ $message->id }}</td>
				<td>{{ $message->name }}</td>
				<td>{{ $message->email }}</td>
				<td>{{ $message->message }}</td>
				<td><form action="/delete-contact/{{ $message->id }}" method="POST"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="{{ csrf_token() }}"><button type="submit">Delete</button></form></td>
			</tr>
		@endforeach
		</table>
		</div>
	</div>
@endsection