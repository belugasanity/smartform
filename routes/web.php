<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/generic', function () {
    return view('generic');
});
Route::get('/elements', function () {
    return view('elements');
});
Route::post('/message', 'MessagesController@store');
Route::get('/forms-one', 'MessagesController@show');
Route::delete('/delete-contact/{id}', 'MessagesController@destroy');

Route::post('elements-save', 'ElementsFormController@store');